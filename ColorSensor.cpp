#include "Arduino.h"
#include "ColorSensor.h"


#pragma region Constructors
ColorSensor::ColorSensor(int newOUT, int newS2, int newS3)
{
	OUT = newOUT;
	S2 = newS2;
	S3 = newS3;

	pinMode(S2, OUTPUT);
	pinMode(S3, OUTPUT);
	pinMode(OUT, INPUT);

}
#pragma endregion Constructors


#pragma region Methods
int ColorSensor::Recognize()
{
	struct HSV hsv = detectorColor();
	double h = hsv.h;

	if (hsv.v < 39.0)
		return Black;
	else if (hsv.s < 15.0 && hsv.v > 40.0)
		return White;
	else {
		if (hsv.h > 300 || hsv.h < 30)
			return Red;
		else if (hsv.h >= 30 && hsv.h < 90)
			return Yellow;
		else if (hsv.h >= 90 && hsv.h < 150)
			return Green;
		else if (hsv.h >= 150 && hsv.h <= 300)
			return Blue;
	}
}

struct HSV ColorSensor::detectorColor()
{
	struct HSV hsv;

	double white = colorRead(WHITE_FILTER);
	double red = colorRead(RED_FILTER);
	double green = colorRead(GREEN_FILTER);
	double blue = colorRead(BLUE_FILTER);

	// Serial.println("1");

	white = 1 / white;
	red = 1 / red;
	green = 1 / green;
	blue = 1 / blue;

	float realRedValue = 1.07*red - 0.30*green + 0.09*blue;
	float realGreenValue = 0 * red + 1.64*green - 0.63*blue;
	float realBlueValue = -0.19*red - 0.56*green + 1.35*blue;

	float realWhiteValue = realRedValue + realGreenValue + realBlueValue;

	white = 1 / realWhiteValue;
	red = 1 / realRedValue;
	green = 1 / realGreenValue;
	blue = 1 / realBlueValue;

	double r, g, b;
	double offset = 3.0 / white;
	r = min(1.0, offset + white / red);
	g = min(1.0, offset + white / green);
	b = min(1.0, offset + white / blue);

	double h, s, v;

	double maxRGB = max(max(r, g), b);
	double minRGB = min(min(r, g), b);
	double delta = maxRGB - minRGB;
	double summa = maxRGB + minRGB;

	v = summa / 2.0;

	if (delta == 0.0) {
		h = s = 0;
	}

	else {
		s = (v < 0.5) ? delta / summa : delta / (2.0 - delta);

		double del_R = (((maxRGB - r) / 6.0) + delta / 2.0) / delta;
		double del_G = (((maxRGB - g) / 6.0) + delta / 2.0) / delta;
		double del_B = (((maxRGB - b) / 6.0) + delta / 2.0) / delta;

		if (r == maxRGB) h = del_B - del_G;
		else if (g == maxRGB) h = 1.0 / 3.0 + del_R - del_B;
		else if (b == maxRGB) h = 2.0 / 3.0 + del_G - del_R;

		if (h < 0)
			h += 1.0;
		if (h > 1)
			h -= 1.0;
	}

	h *= 360.0;
	s *= 100.0;
	v *= 100.0;

	hsv.h = h;
	hsv.s = s;
	hsv.v = v;

	return hsv;
}

double ColorSensor::colorRead(int filter)
{
	setColorFilter(filter);
	
	int sensorDelay = 10;
	double readPulse;
	delay(sensorDelay);

	readPulse = pulseIn(OUT, LOW, 25000000);
	
	if (readPulse < 0.1)
		readPulse = 25000000;

	return readPulse;
}

void ColorSensor::setColorFilter(unsigned char choosenFilter)
{
	digitalWrite(S2, choosenFilter & 0b1);
	digitalWrite(S3, (choosenFilter >> 1) & 0b1);
}
#pragma endregion Methods