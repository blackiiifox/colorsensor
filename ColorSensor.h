/*
	Классы для реализации движения по линии при количестве датчиков от одного до трех.
	Абстрактный класс line_robot определяет общую функциональность объектов. 
	Наследуемые от него классы описывают методы для движения по линии при разных количествах датчиков.
*/
//#include "Arduino.h"
#ifndef ColorSensor_h
#define ColorSensor_h
struct HSV {
	double h;
	double s;
	double v;
};

class ColorSensor {

	public:
		enum { Black, White, Red, Yellow, Green, Blue };
		ColorSensor(int newOUT, int newS2, int newS3);//конструкт для выделения места на переменные
		int Recognize(); // Получение текущего цвета с датчика
		struct HSV detectorColor();

	private:
		double colorRead(int filter);	// Считывание цвета с датчика
		void setColorFilter(unsigned char choosenFilter);	// Установка излучаемого цвета датчика

		int OUT, S2, S3; //Пин
		const int RED_FILTER = 0b00;
		const int WHITE_FILTER = 0b01;
		const int BLUE_FILTER = 0b10;
		const int GREEN_FILTER = 0b11;
		const int UNKNOWN_FILTER = 0b100;

};


#endif