#include <ColorSensor.h>

// Variables
ColorSensor sensor(4, 5, 6); // OUT, S2, S3. S0 - HIGH, S1 - LOW, OE - LOW

void setup(){
  Serial.begin(9600);
}

void loop(){

	switch ( sensor.Recognize() )
	{
			case sensor.Black:
				Serial.println("Black");
				break;
			case sensor.White:
				Serial.println("White");
				break;
			case sensor.Red:
				Serial.println("Red");
				break;
			case sensor.Yellow:
				Serial.println("Yellow");
				break;
			case sensor.Green:
				Serial.println("Green");
				break;
			case sensor.Blue:
				Serial.println("Blue");
				break;
			default:
				Serial.println("Unknown");
				break;
	}
	delay(1000);
}
